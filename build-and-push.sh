#!/bin/bash

repo="aicacia/build-agent-rust"
set -e

function build_and_push() {
  local toolchain=$1
  docker build --build-arg RUST_TOOLCHAIN=${toolchain} -t ${repo}:${toolchain} .
  docker push ${repo}:${toolchain}
  docker tag ${repo}:${toolchain} ${repo}:latest
  docker push ${repo}:latest
}

build_and_push "nightly"
build_and_push "beta"
build_and_push "stable"