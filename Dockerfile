# aicacia/build-agent-rust:${RUST_TOOLCHAIN}

FROM aicacia/docker-kube-helm:19.03-1.17-3.1.2

RUN apt update && \
    apt install -y build-essential

RUN curl https://sh.rustup.rs -sSf | \
    sh -s -- --default-toolchain none -y

ENV PATH=/root/.cargo/bin:$PATH

ARG RUST_TOOLCHAIN=nightly
RUN rustup toolchain install ${RUST_TOOLCHAIN}